
public final class RectAnimationData {
	// 白黒の情報
	public final int [][] input = new int[5][5];

	// コアの位置用の情報
	public final boolean [] coreY = new boolean [5];
	public final boolean [] coreX = new boolean [5];

	public final int moveX;
	public final int moveY;

	public RectAnimationData(int [][] input, boolean [] coreY, boolean [] coreX, int moveX, int moveY) {
		// inputは左上に寄せたものをコピー
		int iNum = 0;
		for(int i=0;i<input.length;i++){
			for(int j=0;j<input[0].length;j++){
				if(input[i][j]!=0){
					for(int k=0;j+k<input[i].length;k++){
						this.input[iNum][k]=input[i][j+k];
					}
					iNum++;
					break;
				}
			}
		}

		for(int i = 0; i < 5; ++i) {
			this.coreY[i] = coreY[i];
			this.coreX[i] = coreX[i];
		}
		this.moveX = moveX;
		this.moveY = moveY;
	}

	public RectAnimationData clone() {
		return new RectAnimationData (input, coreY, coreX, moveX, moveY);
	}
}
