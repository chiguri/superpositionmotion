import java.awt.Button;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class SuperPositionDisplay implements ActionListener{

	//主なフレーム
	JFrame mainFrame;

	//重ねた図形を表示するフレーム
	JFrame menu;

	//MainFrameの描画領域
	MyCanvas c1;
	//MenuFrameの描画領域
	ResultsCanvas c2;

	//スタートボタン・クリアボタン
	Button start_btn,clear_btn;

	//マインフレームの一番奥のパネル
	JPanel p1;

	//左右の入力された矩形を格納
	public int [][] input1 = new int[5][5];
	public int [][] input2 = new int[5][5];
	//↑を計算で使う用にコピー
	public int [][] newinput1 = new int[5][5];
	public int [][] newinput2 = new int[5][5];

	//入力された矩形のsubregion番号を格納
	public int [][] subregion1 = new int[5][5];
	public int [][] subregion2 = new int[5][5];

	//あるblack subregionから見たwite subregionの数
	public int [][] dir1 = new int[5][5];

	//coreに番号をつける
	public int [][] core1 = new int[5][5];

	//あるunit1に重ね合わせが可能なsubregionを判別する
	int [][] successZone = new int[5][5];

	//重ねた図形が最もコンパクトになるようなcoreとsubregionの組み合わせ
	int [][] maxList  = new int[20][3];

	//重ねた図形を格納(10個)
	int [][][] results = new int[10][14][14];

	//細分度(距離)
	int granu=3;

	//アニメーションで移動後のunit2の左上の座標
	int[] moveX= new int[10];
	int[] moveY= new int[10];

	// coreと書いてるけど、縮小するかしないかだよなあ・・・？
	public boolean [] LcoreY = new boolean [5];
	public boolean [] LcoreX = new boolean [5];

	public boolean [] RcoreY = new boolean [5];
	public boolean [] RcoreX = new boolean [5];

	//スタートボタンが押されたらtrueを返す
	public boolean pushStart=false;

	//重ね合わせが成功したらカウント
	int resultCnt;

	public SuperPositionDisplay(){
		//メインフレームを作成
		//起動時に表示されるメインフレーム
		mainFrame = new JFrame() {
			@Override
			public void update(Graphics g) {
				//背景クリアを行わない
				// コメント：overrideしないとクリアされる？
				paint(g);
			}
		};

		final int width = 1070, height = 800;//フレームの大きさ
		final String title = "Superposition";//タイトル
		final Color backc = Color.black;//一番奥の背景色
		final Color panelc= Color.GRAY;//パネルの背景色

		//フレームのタイトル～大きさ～など
		mainFrame.setTitle(title);
		mainFrame.setSize(width,height);
		mainFrame.setBackground(backc);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//×ボタンでプログラムを閉じる

		//一番奥のパネル
		JPanel p1 = new JPanel();
		p1.setBounds(0,0,width,height);
		p1.setBackground(backc);
		p1.setLayout(null);
		mainFrame.add(p1);

		//タイトル(superposition)の画像
		JLabel label = new JLabel();
		ImageIcon icon = new ImageIcon("title.png");
		label.setIcon(icon);
		label.setBounds(180, 10, 1000, 100);
		p1.add(label);

		//格子が書いてある描画領域
		c1 = new MyCanvas();
		c1.setBounds(50,120,950,500);
		c1.setBackground(panelc);
		c1.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent ev) {
				int mou_x=ev.getX();//x座標の保存
				int mou_y=ev.getY();//y座標の保存

				//startボタンを教えてないときだけ
				if(pushStart==false){
					//クリックするたび配列に+1
					pressRec(mou_x, mou_y,input1,50);
					pressRec(mou_x, mou_y,input2,500);
				}
				c1.repaint();
			}
		});
		p1.add(c1);

		//STARTボタン
		start_btn = new Button("START");
		start_btn.setFont(new Font("ＭＳ ゴシック", Font.PLAIN, 40));
		start_btn.setBounds(250, 650, 250, 50);
		start_btn.addActionListener(this);
		start_btn.setForeground(Color.black);
		start_btn.setFocusable(false);
		p1.add(start_btn);

		//CLEARボタン
		clear_btn = new Button("CLEAR");
		clear_btn.setFont(new Font("ＭＳ ゴシック", Font.PLAIN, 40));
		clear_btn.setBounds(550, 650, 250, 50);
		clear_btn.addActionListener(this);
		clear_btn.setForeground(Color.black);
		clear_btn.setFocusable(false);
		p1.add(clear_btn);

	}

	public void start() {
		mainFrame.setVisible(true);
	}

	public void makeMenuFrame(){

		//重ねた図形を表示するウィンドウ

		final Color backc = Color.DARK_GRAY;//一番奥の背景色
		final int menuWidth = 330, menuHeight = 800;//フレームの大きさ

		//menuフレーム
		menu.setBounds(mainFrame.getWidth(),0,menuWidth,menuHeight);
		menu.setBackground(Color.blue);
		//menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//×ボタンでウィンドウを閉じる // だから閉じるな

		//格子が書いてあるキャンバス
		RectAnimationData [] Lrects = new RectAnimationData[10];
		RectAnimationData [] Rrects = new RectAnimationData[10];
		for(int i = 0; i < Lrects.length; ++i) {
			Lrects[i] = new RectAnimationData(input1, LcoreY, LcoreX, 1, 1); // moveX相当はダミーデータ
		}
		for(int i = 0; i < Rrects.length; ++i) {
			Rrects[i] = new RectAnimationData(input2, RcoreY, RcoreX, moveX[i], moveY[i]);
		}
		ResultsCanvas c2 = new ResultsCanvas(results, Lrects, Rrects);
		c2.setBounds(0,0,menuWidth,menuHeight);
		c2.setBackground(backc);
		menu.add(c2);
	}
	public void actionPerformed(ActionEvent ev) {

		//各ボタンをクリックしたときのアクション

		//menuウィンドウを閉じる(nullになる)たびに
		//新しいmeneウィンドウを作成する。
		if(menu == null) {
			menu = new JFrame();
		}
		//startボタンを押した
		if (ev.getSource() == start_btn) {
			if(check()==true){
				pushStart=true;
				calcResults(); // 計算
				//メニューウィンドウを開く
				makeMenuFrame();
				menu.setVisible(true);
			}
		}
		//clearボタンを押した
		if (ev.getSource() == clear_btn) {
			//メニューウィンドウを閉じる(nullに設定)
			menu.dispose();
			menu = null;

			//全部初期化
			Clear();
			c1.repaint();
		}
	}


	class MyCanvas extends Canvas{

		//２つのunitを入力するキャンバス

		public void paint(Graphics g){

			//MyCanvasの格子以外の情報(文字とか)を描画
			paintText(g);

			//格子の描画
			paintRect(g);
			paintRect(g);

			//inputの数字を見て格子を色塗り
			paintColor(g,input1,50);
			paintColor(g,input2,500);
		}

		private void paintRect(Graphics g){

			//格子を描画する

			int x=50;
			int y=50;
			int w=80;
			int x_sa=450;
			int x2=x;
			int y2=y;

			for (int i=1; i<=5; i++) {
				for (int j=1; j<=5; j++) {
					g.setColor(Color.DARK_GRAY);
					g.drawRect(x,y,w,w);
					g.drawRect(x+x_sa,y,w,w);
					x=x+w;
				}
				x=x2;
				y=y+w;
			}
			y=y2;
		}

		private void paintText(Graphics g){

			//"Uniit1"や"Unit2"などのテキストを描画

			g.setColor(Color.white);
			g.fillRect(50, 465, 50, 20);
			g.setColor(Color.black);
			g.fillRect(250, 465, 50, 20);

			//テキスト描画(いろいろ)
			Graphics2D g2 = (Graphics2D)g;
			g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
	                        RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			g2.setColor(Color.black);
			Font font1 = new Font("ＭＳ 明朝", Font.BOLD, 20);
			g2.setFont(font1);
			g2.drawString(":important", 100, 480);
			g2.drawString(":not important", 300, 480);

			Font font2 = new Font("ＭＳ 明朝", Font.BOLD, 40);
			g2.setFont(font2);
			g2.drawString("Unit1", 200, 40);
			g2.drawString("Unit2", 645, 40);
		}

		private void paintColor(Graphics g,int[][] array,int x){

			//配列の中を見て、矩形を色塗り

			int y=50;
			int w=80;

			for(int i = 0; i < 5; ++i) {
				for(int j = 0; j < 5; ++j) {
					//---左の矩形---
					if(array[i][j] == 1) {
						g.setColor(Color.white);
						g.fillRect(x + w * j, y + w * i,w,w);
					}
					if(array[i][j] == 2) {
						//g.setColor(Color.DARK_GRAY);
						g.setColor(Color.black);
						g.fillRect(x + w * j, y + w * i,w,w);
					}
				}
			}
		}

	}


	public void calcResults(){

		//入力された矩形の情報が確定後
		//重ね合わせが成功するかの計算をする。

		//入力された矩形をnewinputにコピー
		copy(input1,newinput1);
		copy(input2,newinput2);

		//入力された配列の要素を左上に寄せる
		yoseruArray(newinput1);
		yoseruArray(newinput2);

		//重複した列の削除(定性的に扱うため最小限のsubregionで扱う) Weightって幅（width）じゃなくて体重じゃ・・・ダイエットしたいけど違うだろ・・・
		slimWeight(newinput1,1);
		slimWeight(newinput2,2);

		//重複した行の削除(定性的に扱うため最小限のsubregionで扱う)
		slimHeight(newinput1,1);
		slimHeight(newinput2,2);

		//subregionに番号を割り当てる
		regionNumber(newinput1,subregion1);
		regionNumber(newinput2,subregion2);

		//coreを見つけ、番号を割り当てる
		coreFind(newinput1,subregion1,dir1,core1);

		//重ね合わせを行い、失敗なら新しいcoreを探す
		for(int i=0;i<3;i++){
			//重ね合わせ失敗
			if(checkSuperposition(returnLengthH(newinput2),returnLengthW(newinput2))==false){
				//細分度(距離)をあげてcoreを探しなおす
				granu+=2;
				coreFind(newinput1,subregion1,dir1,core1);
			}
			else{
				//配列の初期化
				arrayZero(successZone);
				break;
			}
		}

		//coreのW集合から計算し、成功/失敗や
		//coveredを計算する。
		if(checkSuperposition2(returnLengthH(newinput2),returnLengthW(newinput2))==false){//失敗

			//アラート表示
			JLabel label = new JLabel("重ね合わせ失敗です");
			label.setFont(new Font("ＭＳ ゴシック", Font.BOLD, 30));
			label.setForeground(Color.RED);
			JOptionPane.showMessageDialog(mainFrame, label);

			//メニューウィンドウを閉じる(nullに設定)
			menu.dispose();
			menu = null;

			//配列の初期化
			Clear();
			c1.repaint();

		}
		else{//重ね合わせが成功する

			//maxListの整理
			maxListSort();

			//重ね合わせの実行
			makeResultList(returnLengthH(newinput2),returnLengthW(newinput2),newinput2);
		}
	}
	public void Clear(){

		//いろんな配列や変数、ボタンの初期化

		//配列の初期化
		arrayZero(input1);
		arrayZero(input2);
		arrayZero(newinput1);
		arrayZero(newinput2);
		arrayZero(subregion1);
		arrayZero(subregion2);
		arrayZero(dir1);
		arrayZero(core1);
		arrayZero(maxList);
		arrayZero(successZone);

		for(int i = 0; i < 10; ++i) {
			arrayZero(results[i]);
		}

		//startボタンをfalse
		pushStart=false;
		//細分度(距離)の初期化
		granu=3;

		for(int i=0;i<LcoreY.length;i++){
			LcoreY[i]=false;
			LcoreX[i]=false;
			RcoreY[i]=false;
			RcoreX[i]=false;
		}
		resultCnt=0;
	}

	public boolean SameOrNot(int [][]array1,int [][] array2){

		//array1とarray2が同じならtrueを返す

		for(int i=0;i<array1.length;i++){
			for(int j=0;j<array1[0].length;j++){
				if(array1[i][j] != array2[i][j]) {
					return false;
				}
			}
		}
		return true;//全部同じ
	}

	public boolean existResults(int [][] coveredZone) {
		for(int i = 0; i < resultCnt; ++i) {
			if(SameOrNot(coveredZone, results[i])) {
				return true;
			}
		}
		return false;
	}

	public void makeResultList(int unit2_h,int unit2_w,int[][] unit2){

		//maxList(重ね合わせが成功するcoreとsubregionの組み合わせ)を見て
		//重ねた図形をresultに格納していく。


		//maxListの中に入ってる情報の数
		int listCnt=0;
		for(int i=0;i<maxList.length;i++){
			if(maxList[i][0]!=0){
				listCnt++;
			}
		}

		for(int r=0;r<listCnt;r++){
			int coreNmber=maxList[r][0];
			int subNmber=maxList[r][1];

			//successZoneの作成
			makeSuccessZone(unit2_w, unit2_h);
			//あるcoreに対し、成功するsubregionを探す
			for(int i=0;i<newinput2.length;i++){
				for(int j=0;j<newinput2[0].length;j++){
					if(core1[i][j]==coreNmber){//coreが・のやつに限定
						successZonePlus(i,j,unit2_w,unit2_h);
					}
				}
			}

			//重なっているsubregionの数
			int [][] coveredZone = new int[14][14];
			int [][] successZoneNmber = new int[5][5];

			//coreの配列番号
			int core_i=0;
			int core_j=0;
			Loop:
			for(int i=0;i<core1.length;i++){
				for(int j=0;j<core1[0].length;j++){
					if(core1[i][j]==coreNmber){
						core_i=i;
						core_j=j;
						break Loop;
					}
				}
			}

			//successZoneに番号付け
			int subCnt=0;
			for(int i=0;i<successZone.length;i++){
				for(int j=0;j<successZone[0].length;j++){
					if(successZone[i][j]==2){
						subCnt++;
						successZoneNmber[i][j]=subCnt;
					}
				}
			}

			//coveredZoneの初期化
			arrayZero(coveredZone);
			//coveredZoneのにunit1をセット
			for(int i=0;i<newinput1.length;i++){
				for(int j=0;j<newinput1[0].length;j++){
					coveredZone[i+4][j+4]=newinput1[i][j];
				}
			}
			//coreに対応するsubregionの座標
			int sub_i=0;
			int sub_j=0;
			Loop:
			for(int i=0;i<successZoneNmber.length;i++){
				for(int j=0;j<successZoneNmber[0].length;j++){
					if(successZoneNmber[i][j]==subNmber){
						sub_i=i;
						sub_j=j;
						break Loop;
					}
				}
			}

			//coveredZoneのにunit2をセット
			for(int i=0;i<unit2_h;i++){
				for(int j=0;j<unit2_w;j++){
				coveredZone[i+core_i-sub_i+4][j+core_j-sub_j+4]=unit2[i][j];
				}
			}
			//重ねた図形を順番にresultに格納していく
			if(resultCnt < 10 && !existResults(coveredZone)) {
				copy(coveredZone, results[resultCnt]);
				moveX[resultCnt]=core_j-sub_j+4;
				moveY[resultCnt]=core_i-sub_i+4;
				resultCnt++;
			}
		}
	}

	/*
	public void yoseruArray2(int[][] array){

		//配列の要素を左上につめる

		int[][] inpcopy = new int[array.length][array[0].length];

		int array_i=0;
		Loop:
		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[0].length;j++){
				if(array[i][j]!=0){
					array_i=i;
					break Loop;
				}
			}
		}
		int array_j=0;
		Loop1:
		for(int j=0;j<array[0].length;j++){
			for(int i=0;i<array.length;i++){
				if(array[i][j]!=0){
					array_j=j;
					break Loop1;
				}
			}
		}

		for(int i=0;i<array.length-array_i;i++){
			for(int j=0;j<array[0].length-array_j;j++){
				inpcopy[i][j]=array[i+array_i][j+array_j];
			}
		}
		copy(inpcopy,array);
	}
	*/
	public void maxListSort(){

		//maxListの中で最大のcoveredのものだけをリストに残す。

		int[][] copyList = new int[maxList.length][3];

		int minCovered=0;
		int start=50;

		//最大のcovred=重ねた図形のsubregionの数が最小なので、
		//リストの中から最小の値を求める。
		for(int i=0;i<maxList.length;i++){
			if(maxList[i][2]!=0){
				minCovered=Math.min(start, maxList[i][2]);
				start=minCovered;
			}
		}

		//上記で求めた最小の値のみをmaxListに格納
		int copyCnt=0;
		for(int i=0;i<maxList.length;i++){
			if(maxList[i][2]==minCovered){
				copyList[copyCnt][0]=maxList[i][0];
				copyList[copyCnt][1]=maxList[i][1];
				copyList[copyCnt][2]=maxList[i][2];
				copyCnt++;
			}
		}
		copy(copyList,maxList);
	}
	public void covered(int coreNmber,int unit2_w,int unit2_h){

		//重ねた図形のsubregionの数

		int [][] coveredZone = new int[14][14];
		int [][] successZoneNmber = new int[5][5];

		//coreの座標
		int core_i=0;
		int core_j=0;
		Loop:
		for(int i=0;i<core1.length;i++){
			for(int j=0;j<core1[0].length;j++){
				if(core1[i][j]==coreNmber){
					core_i=i;
					core_j=j;
					break Loop;
				}
			}
		}

		//coreに対応するsubregionに番号付け
		int subCnt=0;
		for(int i=0;i<successZone.length;i++){
			for(int j=0;j<successZone[0].length;j++){
				if(successZone[i][j]==2){
					subCnt++;
					successZoneNmber[i][j]=subCnt;
				}
			}
		}

		int [][] coveredList = new int[subCnt][2];

		int cnt=1;
		for(int i=0;i<coveredList.length;i++){
			coveredList[i][0]=cnt;
			cnt++;
		}

		for(int s=1;s<=subCnt;s++){

			//coveredZoneの初期化
			arrayZero(coveredZone);
			//coveredZoneのにunit1をセット
			for(int i=0;i<newinput1.length;i++){
				for(int j=0;j<newinput1[0].length;j++){
					coveredZone[i+4][j+4]=newinput1[i][j];
				}
			}
			//coreに対応するsubregionの座標
			int sub_i=0;
			int sub_j=0;
			Loop:
			for(int i=0;i<successZoneNmber.length;i++){
				for(int j=0;j<successZoneNmber[0].length;j++){
					if(successZoneNmber[i][j]==s){
						sub_i=i;
						sub_j=j;
						break Loop;
					}
				}
			}
			//coveredZoneのにunit2をセット
			for(int i=0;i<unit2_h;i++){
				for(int j=0;j<unit2_w;j++){
				coveredZone[i+core_i-sub_i+4][j+core_j-sub_j+4]=successZone[i][j];
				}
			}

			//covered計算
			int coveredCnt=0;
			for(int i=0;i<coveredZone.length;i++){
				for(int j=0;j<coveredZone[0].length;j++){
					if(coveredZone[i][j]!=0){
						coveredCnt++;
					}
				}
			}
			coveredList[s-1][1]=coveredCnt;
		}

		int maxCovered=0;
		int start=100;
		for(int i=0;i<coveredList.length;i++){
			maxCovered=Math.min(start,coveredList[i][1]);
			start=maxCovered;
		}

		int cntMaxcovered=0;
		for(int i=0;i<coveredList.length;i++){
			if(coveredList[i][1]==maxCovered){
				cntMaxcovered++;
			}
		}
		int startMaxList=0;
		for(startMaxList=0;startMaxList<maxList.length;startMaxList++){
			if(maxList[startMaxList][0]==0){
				break;
			}
		}

		//0列にcore番号
		for(int i=startMaxList;i<cntMaxcovered+startMaxList;i++){
			maxList[i][0]=coreNmber;
		}
		//1列にsubregion番号,3列にcovered数
		int maxSubCnt=startMaxList;
		for(int i=0;i<coveredList.length;i++){
			if(coveredList[i][1]==maxCovered){
				maxList[maxSubCnt][1]=coveredList[i][0];
				maxList[maxSubCnt][2]=maxCovered;
				maxSubCnt++;
			}
		}
	}
	public boolean checkSuperposition(int unit2_h,int unit2_w){

		//重ね合わせの成功失敗を判別

		int coreCnt=0;
		for(int i=0;i<core1.length;i++){
			for(int j=0;j<core1[0].length;j++){
				if(core1[i][j]!=0){
					coreCnt++;
				}
			}
		}

		int coreNmber=1;
		int failCnt=0;
		for(coreNmber=1;coreNmber<=coreCnt;coreNmber++){
			//successZoneの作成
			makeSuccessZone(unit2_w, unit2_h);
			//あるcoreに対し、成功するsubregionを探す
			for(int i=0;i<newinput1.length;i++){
				for(int j=0;j<newinput1[0].length;j++){
					if(core1[i][j]==coreNmber){//coreが・のやつに限定
						successZonePlus(i,j,unit2_w,unit2_h);
					}
				}
			}

			//coreに対して成功するsubregionが存在するか確認
			int successCnt=0;
			for(int i=0;i<successZone.length;i++){
				for(int j=0;j<successZone[0].length;j++){
					if(successZone[i][j]==2){
						successCnt++;
					}
				}
			}
			if(successCnt==0){
				failCnt++;
			}
			if(failCnt==coreCnt){//重ね合わせ失敗
				return false;
			}
		}
		return true;
	}
	public boolean checkSuperposition2(int unit2_h,int unit2_w){

		//重ね合わせの成功失敗を判別

		int coreCnt=0;
		for(int i=0;i<core1.length;i++){
			for(int j=0;j<core1[0].length;j++){
				if(core1[i][j]!=0){
					coreCnt++;
				}
			}
		}

		int coreNmber=1;
		int failCnt=0;
		for(coreNmber=1;coreNmber<=coreCnt;coreNmber++){
			//successZoneの作成
			makeSuccessZone(unit2_w, unit2_h);
			//あるcoreに対し、成功するsubregionを探す
			for(int i=0;i<newinput1.length;i++){
				for(int j=0;j<newinput1[0].length;j++){
					if(core1[i][j]==coreNmber){//coreが・のやつに限定
						successZonePlus(i,j,unit2_w,unit2_h);
					}
				}
			}

			//coreに対して成功するsubregionが存在するか確認
			int successCnt=0;
			for(int i=0;i<successZone.length;i++){
				for(int j=0;j<successZone[0].length;j++){
					if(successZone[i][j]==2){
						successCnt++;
					}
				}
			}
			if(successCnt==0){
				failCnt++;
			//	System.out.println("重ね合わせは失敗！！！！");
			}
			else{
				//coverd計算
				covered(coreNmber,unit2_w,unit2_h);
			}
			if(failCnt==coreCnt){//重ね合わせ失敗
				return false;
			}
		}
		return true;
	}
	public static void arrayTwo(int[][] array){

		//二次元配列の初期化

		for(int i = 0; i < array.length; ++i) {
			for(int j = 0; j < array[0].length; ++j) {
				array[i][j]=2;
			}
		}
	}
	public void makeSuccessZone(int unit2_w,int unit2_h){

		//unit1に対して重ね合わせが成功するsubregionを
		//表示するsuccessZoneの作成


		//sccessZoneの初期化
		arrayTwo(successZone);

		//successZone作成
		for(int i=unit2_h;i<successZone.length;i++){
			for(int j=0;j<successZone[0].length;j++){
				successZone[i][j]=9;
			}
		}
		for(int i=0;i<successZone.length;i++){
			for(int j=unit2_w;j<successZone[0].length;j++){
				successZone[i][j]=9;
			}
		}
	}
	public void successZonePlus(int s,int t,int unit2_w,int unit2_h){

		//上、下、右、左、左上・・・の方向に白があるかを確認し、
		//重ね合わせが失敗するsubregionには9を格納する。
		//(ex.上1に白があるならば、unit2の2行目から下にはすべて9をいれる。)

		for(int i=0;i<newinput1.length;i++){
			for(int j=0;j<newinput1[0].length;j++){
				if(newinput1[i][j]==1){
					//up-right
					if(i<s && j>t){
					//	System.out.println("up"+(s-i)+"right"+(j-t));
						uprightFind((s-i),(j-t),unit2_w);
					}
					//down-right
					else if(i>s && j>t){
					//	System.out.println("down"+(i-s)+"right"+(j-t));
						downrightFind((i-s),unit2_h,(j-t),unit2_w);
					}
					//down-left
					else if(i>s && j<t){
					//	System.out.println("down"+(i-s)+"left"+(t-j));
						downleftFind((i-s),unit2_h,(t-j));
					}
					//up-left
					else if(i<s && j<t){
					//	System.out.println("up"+(s-i)+"left"+(t-j));
						upleftFind((s-i),(t-j));
					}
					//up
					else if(i<s && j==t){
					//	System.out.println("up"+(s-i));
						upFind(s-i);
					}
					//down
					else if(i>s && j==t){
					//	System.out.println("down"+(i-s));
						downFind(i-s,unit2_h);
					}
					//right
					else if(j>t && i==s){
					//	System.out.println("right"+(j-t));
						rightFind(j-t,unit2_w);
					}
					//left
					else if(j<t && i==s){
					//	System.out.println("left"+(t-j));
						leftFind(t-j);
					}
					else{;}
				}
			}
		}
	}
	public void downleftFind(int downN ,int unit2_h,int leftN){

		//左下に白を発見。
		//重ね合わせが失敗するsubregionに9を格納。

		for(int i=0;i<unit2_h-downN;i++){
			for(int j=leftN;j<successZone[0].length;j++){
				successZone[i][j]=9;
			}
		}
	}
	public void upleftFind(int upN,int leftN){

		//左上・・・以下同様

		for(int i=upN;i<successZone.length;i++){
			for(int j=leftN;j<successZone[0].length;j++){
				successZone[i][j]=9;
			}
		}
	}
	public void downrightFind(int downN ,int unit2_h,int rightN,int unit2_w){

		for(int i=0;i<unit2_h-downN;i++){
			for(int j=0;j<unit2_w-rightN;j++){
				successZone[i][j]=9;
			}
		}
	}
	public void uprightFind(int upN,int rightN,int unit2_w){

		for(int i=upN;i<successZone.length;i++){
			for(int j=0;j<unit2_w-rightN;j++){
				successZone[i][j]=9;
			}
		}
	}
	public void rightFind(int n,int unit2_w){

		for(int j=0;j<unit2_w-n;j++){
			for(int i=0;i<successZone.length;i++){
				successZone[i][j]=9;
			}
		}
	}
	public void leftFind(int n){

		for(int j=n;j<successZone[0].length;j++){
			for(int i=0;i<successZone.length;i++){
				successZone[i][j]=9;
			}
		}
	}
	public void upFind(int n){

		for(int i=n;i<successZone.length;i++){
			for(int j=0;j<successZone[0].length;j++){
				successZone[i][j]=9;
			}
		}
	}
	public void downFind(int n,int unit2_h){

		for(int i=0;i<unit2_h-n;i++){
			for(int j=0;j<successZone[0].length;j++){
				successZone[i][j]=9;
			}
		}
	}
	public int returnLengthH(int[][]array){

		//unit2の矩形の縦の長さ

		int h_Length=0;

		for(int j=0;j<array[0].length;j++){
			for(int i=0;i<array.length;i++){
				if(array[i][j]!=0){
					h_Length++;
				}
			}
			if(h_Length!=0){
				break;
			}
		}
		return h_Length;
	}
	public int returnLengthW(int[][]array){

		//unit2の矩形の横の長さ

		int w_Length=0;

		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[0].length;j++){
				if(array[i][j]!=0){
					w_Length++;
				}
			}
			if(w_Length!=0){
				break;
			}
		}
		return w_Length;
	}
	public void coreFind(int[][]array,int[][]subarray,int[][]dirarray,int[][] corearray){

		//細分度(距離)を3,5,7...と上げていきながら
		//coreを探し、複数ある場合は区別できるように
		//番号を1,2..つける。

		for(int i=granu;i<=9;i+=2){

			granuDirWhite(i,newinput1,subregion1,dir1);//dirの最大値
			if(unitMaxWhite(newinput1)==dirMax(dir1)){//unitのwhite総数=dirの最大値ならばcore発見
				coreNumber(newinput1,dir1,core1);//coreに番号つける
				break;
			}
		}
	}
	public void coreNumber(int[][]array,int[][]dirarray,int[][]corearray){

		//coreに番号を振る

		int number=1;

		for(int i=0;i<dirarray.length; i++){
			for(int j=0;j<dirarray[0].length; j++){
				if(dirarray[i][j]==unitMaxWhite(array)){
					corearray[i][j]=number;
					number++;
				}
			}
		}
	}
	public int unitMaxWhite(int[][]array){

		//unit内のwhiteの総数

		int cnt=0;

		for(int i = 0; i<array.length; i++){
			for(int j = 0;j<array[0].length; j++){
				if(array[i][j]==1){
					cnt++;
				}
			}
		}
		return cnt;
	}
	public int granuDirWhite(int granu,int[][]array,int[][]subarray,int[][]dirarray){

		//ある細分度(細分度)の時の|dir(t_{i,j},W)|の数を
		//dir配列に入れる。

		int [] white = new int[26];//重複してwhiteをカウントしないために配列で管理
		int w_num=0;//配列の番号(whiteの番号)
		int cnt;//white数をカウント

		for(int k=0;k<26;k++){
			cnt=0;
			for(int i =0; i<array.length; i++){
				for(int j=0;j<array[0].length; j++){
					for(int s=-granu/2;s<=granu/2;s++){
						for(int t=-granu/2;t<=granu/2;t++){
							if(array[i][j]==2 && subarray[i][j]==k){
								if(i+s>=0 && j+t>=0 && i+s<array.length && j+t<array[0].length){
									if(array[i+s][j+t]==1){
										w_num = subarray[i+s][j+t];
										white[w_num]=1;
									}
								}
							}
						}
					}
				}
			}
			for(int f=0;f<26;f++){//whiteを数える
				if(white[f]==1){
					cnt++;
				}
			}
			for(int i = 0; i<array.length; i++){//dirへの記入
				for(int j = 0;j<array[0].length; j++){
					if(array[i][j]==2 && subarray[i][j]==k){
						dirarray[i][j] = cnt;
					}
				}
			}
			for(int i = 0; i<white.length; i++){//whiteの初期化
				white[i]=0;
			}
		}
		return granu;
	}
	public int dirMax(int[][]dirarray){

		//配列dirの中で最大の値を返す。
		//つまりある細分度の時の最大の|dir(t_{i,j},W)|

		int start=0;//とりあえず初期値0を入れた
		int ans=0;//|dir(t_{i,j},W)|の最大値を格納

		for(int i = 0; i<dirarray.length; i++){
			for(int j = 0; j<dirarray[0].length; j++){
				ans=Math.max(start,dirarray[i][j]);
				start=ans;
			}
		}
		return ans;
	}
	public void regionNumber(int[][]array,int[][]subarray){

		//subregionに番号を割り当てる

		int [][] big_input = new int[9][9];

		//入力を[5]*[5]→[9]*[9]に変更
		dir_big_input(array,big_input);
		//subregionに番号を割り当てる
		dir_num(big_input,subarray);
	}
	public void dir_num(int[][] big_input,int[][] subregion){

		//big_inputの情報からsubregionに番号付け

		int start_w=0;
		int start_h=0;

		//---矩形の左上の座標を取得---
		Loop:
		for(int i=0;i<big_input.length; i++){
			for(int j=0;j<big_input[0].length; j++){
				if(big_input[i][j]==1 || big_input[i][j]==2){
					start_w=j;
					start_h=i;
					break Loop;
				}
			}
		}

		//---矩形の右下のx座標を取得---
		int end_w=0;
		for(int j=start_w;j<big_input[0].length; j++){
			end_w=j;
			if(big_input[start_h][j]==0){
				end_w-=2;
				break;
			}
		}

		//---矩形の右下のy座標を取得---
		int end_h=0;
		for(int i=start_h;i<big_input.length; i++){
			end_h=i;
			if(big_input[i][start_w]==0){
				end_h-=2;
				break;
			}
		}

		int number=0;//regionの番号
		/*
		 * big_inputの情報からsubregionに番号付け
		 */
		for(int i = start_h/2; i<end_h/2+1; i++){
			for(int j = start_w/2; j<end_w/2+1; j++){
				if(subregion[i][j]==0){
					number++;
					subregion[i][j]=number;

					if(j*2+1<end_w &&big_input[i*2][j*2+1]==3){
						for(int s=0;(j*2)+(s*2)+1<end_w;s++){
							if(big_input[i*2][(j*2)+(s*2)+1]==3){
								subregion[i][j+s+1]=number;
								//下も
								for(int t=0;(i*2)+(t*2)+1<end_h;t++){
									if(big_input[(i*2)+(t*2)+1][j*2]==3){
										subregion[i+t+1][j]=number;
										subregion[i+t+1][j+s+1]=number;
									}
								}
							}
						}
					}
					else if(i*2+1<end_h && big_input[i*2+1][j*2]==3){
						for(int s=0;(i*2)+(s*2)+1<end_h;s++){
							if(big_input[(i*2)+(s*2)+1][j*2]==3){
								subregion[i+s+1][j]=number;
							}
						}
					}
					else{;}
				}
			}
		}
	}
	public void dir_big_input(int[][] array, int[][] big_input){


		// 入力されたinput[5]*[5]の配列を
		// big_input[9]*[9]の配列に変更

		//[9]*[9]の偶数番号に[5]*[5]の要素を
		//順番に入れていく
		for(int i = 0; i<array.length; i++){
			for(int j = 0; j<array[0].length; j++){
				big_input[i*2][j*2] = array[i][j];
			}
		}

		//big_inputの隣接する要素を順番に確認していき
		//同じ数字ならば3、異なる数字ならば9を配列の
		//奇数番号に入れる。

		//9:regionを区別するラインになる
		//3:どうでもいい

		for(int s=0; s<array.length; s++){
			for(int t=0; t<(array.length-1); t++){
				if(array[s][t]!=array[s][t+1]){
					for(int u=0;u<big_input.length;u++){
						big_input[u][1+2*t]=9;
					}
				}
				else{
					for(int u=0;u<big_input.length;u++){
						if(big_input[u][1+2*t]!=9){
							big_input[u][1+2*t]=3;
						}
					}
				}
			}
		}
		for(int s=0; s<array[0].length; s++){
			for(int t=0; t<(array[0].length-1); t++){
				if(array[t][s]!=array[t+1][s]){
					for(int u=0;u<big_input[0].length;u++){
						big_input[1+2*t][u]=9;
					}
				}
				else{
					for(int u=0;u<big_input[0].length;u++){
						if(big_input[1+2*t][u]!=9){
							big_input[1+2*t][u]=3;
						}
					}
				}
			}
		}
	}
	public void slimHeight(int[][] array,int num){
		//重複した行の削除

		int a = array.length-1;
		int [] number = new int[a];

		//連続して同じ配列の行があったとき、
		//その行はすべて0にする
		for(int i=0;i<array.length-1;i++){
			int cnt=0;
			int zeroCnt=0;
			for(int j=0;j<array[0].length;j++){
				if(array[i][j]==array[i+1][j]){
					cnt++;

					if(array[i][j]==0&&array[i+1][j]==0){
						zeroCnt++;
					}

					if(zeroCnt!=cnt&&cnt==array[0].length){

						number[i]=1;

						for(int s=0;s<array[0].length;s++){
							array[i][s]=0;
						}
					}
				}
			}
		}


		for(int i=a-1;i>=0;i--){
			if(number[i]==1){

				if(num==1){
					if(i==4){
						LcoreY[4]=true;
					}
					if(i==3){
						LcoreY[3]=true;
					}
					if(i==2){
						LcoreY[2]=true;
					}
					if(i==1){
						LcoreY[1]=true;
					}
					if(i==0){
						LcoreY[0]=true;
					}
				}
				if(num==2){
					if(i==4){
						RcoreY[4]=true;
					}
					if(i==3){
						RcoreY[3]=true;
					}
					if(i==2){
						RcoreY[2]=true;
					}
					if(i==1){
						RcoreY[1]=true;
					}
					if(i==0){
						RcoreY[0]=true;
					}
				}
			}
		}

		int[][] arraycopy = new int[array.length][array[0].length];
		int height=0;

		//0だけの行以外をarraycopyにいれる
		for(int k=0;k<array.length;k++){
			if(array[k][0]!=0){
				for(int s=0;s<array[0].length;s++){
					arraycopy[height][s]=array[k][s];
				}
				height++;
			}
		}
		copy(arraycopy,array);
	}
	public void slimWeight(int[][] array,int num){
		//重複した列の削除

		int a = array[0].length-1;
		int [] number = new int[a];

		//連続して同じ配列の列があったとき、
		//その列はすべて0にする
		for(int i=0;i<array[0].length-1;i++){
			int cnt=0;
			int zeroCnt=0;
			for(int j=0;j<array.length;j++){
				if(array[j][i]==array[j][i+1]){
					cnt++;

					if(array[j][i]==0&&array[j][i+1]==0){
						zeroCnt++;
					}

					if(zeroCnt!=cnt&&cnt==array.length){

						number[i]=1;

						for(int s=0;s<array.length;s++){
							array[s][i]=0;
						}
					}
				}
			}
		}

		for(int i=a-1;i>=0;i--){
			if(number[i]==1){
				if(num==1){
					if(i==4){
						LcoreX[4]=true;
					}
					if(i==3){
						LcoreX[3]=true;
					}
					if(i==2){
						LcoreX[2]=true;
					}
					if(i==1){
						LcoreX[1]=true;
					}
					if(i==0){
						LcoreX[0]=true;
					}
				}
				if(num==2){
					if(i==4){
						RcoreX[4]=true;
					}
					if(i==3){
						RcoreX[3]=true;
					}
					if(i==2){
						RcoreX[2]=true;
					}
					if(i==1){
						RcoreX[1]=true;
					}
					if(i==0){
						RcoreX[0]=true;
					}
				}
			}
		}


		int[][] arraycopy = new int[array.length][array[0].length];
		int weight=0;

		//0だけの列以外をarraycopyにいれる
		for(int k=0;k<array[0].length;k++){
			if(array[0][k]!=0){
				for(int s=0;s<array.length;s++){
					arraycopy[s][weight]=array[s][k];
				}
				weight++;
			}
		}
		copy(arraycopy,array);
	}
	public void yoseruArray(int[][]array){

		//配列の要素を左上につめる

		int[][] inpcopy = new int[array.length][array[0].length];

		int iNum=0;

		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[0].length;j++){
				if(array[i][j]!=0){
					for(int k=0;j+k<array.length;k++){
						inpcopy[iNum][k]=array[i][j+k];
					}
					iNum++;
					break;
				}
			}
		}

		copy(inpcopy,array);
	}
	public static void copy(int[][] array,int[][] newarray){
		//array→newarray
		//二次元配列のコピー
		for(int i=0;i<array.length;i++){
			newarray[i] = new int[array[i].length];
			System.arraycopy(array, 0, newarray, 0, array.length);
		}
	}
	public void arrayZero(int[][] array){
		//配列の初期化
		for(int i = 0; i < array.length; ++i) {
			for(int j = 0; j < array[0].length; ++j) {
				array[i][j]=0;
			}
		}
	}
	public boolean check(){

		//入力された矩形の情報をチェック
		//問題があればエラー表示

		//配列の中の要素が矩形の並びになっているかチェック
		if(checkRectangle(input1)==false || checkRectangle(input2)==false){

			//アラート表示
			JLabel label = new JLabel("矩形のunitを2つ入力してください");
			label.setFont(new Font("ＭＳ ゴシック", Font.BOLD, 30));
			label.setForeground(Color.RED);
			JOptionPane.showMessageDialog(mainFrame, label);

			return false;
		}
		//配列の中の要素に白と黒の両方があるかチェック
		else if(checkColor(input1)==false || checkColor(input2)==false){

			//アラート表示
			JLabel label = new JLabel("unitの内部はwhiteとblackの両方が必要です。");
			label.setFont(new Font("ＭＳ ゴシック", Font.BOLD, 30));
			label.setForeground(Color.RED);
			JOptionPane.showMessageDialog(mainFrame, label);

			return false;
		}
		else{
			return true;
		}
	}
	public boolean checkColor(int[][]array){

		//配列に入力された矩形の情報が
		//whiteとblackの両方であるかチェック

		Boolean whiteColor = false;
		Boolean blackColor = false;

		//配列にwhiteとblackがあるか判定
		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[0].length;j++){
				if(array[i][j]==1){
					whiteColor=true;
				}
				if(array[i][j]==2){
					blackColor=true;
				}
			}
		}
		if(whiteColor==true && blackColor==true){
			return true;
		}

		return false;
	}
	public boolean checkRectangle(int[][] array){

		//配列に入力された情報が矩形になっているかチェック

		int srt_i=0;//矩形の左上の配列のi
		int srt_j=0;//矩形の左上の配列のj

		//矩形の左上の配列の情報を取得
		Loop:
			for(int i=0;i<array.length;i++){
				for(int j=0;j<array[0].length;j++){
					if(array[i][j]==1 || array[i][j]==2){
						srt_i=i;
						srt_j=j;
						break Loop;
					}
				}
			}

		int end_i=0;//矩形の右下の配列のi
		int end_j=0;//矩形の右下の配列のj

		//矩形の右下の配列の情報を取得
		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[0].length;j++){
				if(array[i][j]==1 || array[i][j]==2){
					end_i=i;
					end_j=j;
				}
			}
		}

		//矩形の左上と右下で囲まれる領域がすべて1or2(何か塗られている)であり、
		//その他の領域がすべて0(何も書かれていない)であることを確かめる

		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[0].length;j++){
				if(i>=srt_i && i<=end_i && j>=srt_j && j<=end_j){
					if(array[i][j]!=1 && array[i][j]!=2){
						return false;
					}
				}
				else{
					if(array[i][j]!=0){
						return false;
					}
				}
			}
		}
		return true;
	}

	public void pressRec(int m_x, int m_y,int[][]array,int x){

		//格子の中をクリックするたびに
		//配列に1を足していく(0→1→2→0...)

		//0:何もない
		//1:white
		//2:black
		int y=50;
		int w=80;

		Loop1:
		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[0].length;j++){
				if(m_x>x && m_y>y){
					if(m_x<=(x+w)+(w*j) && m_y<=(y+w)+(w*i)){
						array[i][j]+=1;
						if(array[i][j]>2){
							array[i][j]=0;
						}
						break Loop1;
					}
				}
			}
		}
	}
	public static void print_array(int[][] array){

		//二次元配列の表示

		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[0].length;j++){
				System.out.print(array[i][j]);
			}System.out.println();
		}
		System.out.println();
	}

}

