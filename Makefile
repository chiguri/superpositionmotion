.PHONY: clean main all test

JAVA_SOURCE := $(shell ls *.java)
MAIN_CLASS := Main
ENCODING := utf8

main: $(MAIN_CLASS).class ;

all: main ;

test: $(MAIN_CLASS).class
	java -classpath . $(MAIN_CLASS)



$(MAIN_CLASS).class: $(JAVA_SOURCE)
	javac -encoding $(ENCODING) $(JAVA_SOURCE)



clean:
	rm -f *.class
