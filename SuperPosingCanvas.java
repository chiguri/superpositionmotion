import java.awt.BasicStroke;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

class SuperPosingCanvas extends Canvas implements Runnable{

	//アニメーションの描画領域

	Image back;
	Dimension size;
	Graphics buffer;

	//アニメーションウィンドウで初めにunitが2つ表示される際の
	//unit1の左上の座標(LeftX,LeftY)とunit2の左上の座標(RightX,RightY)
	int LeftX=0;
	int LeftY=0;
	int RightX=0;
	int RightY=0;

	//unit1,2の各マスごとの値
	//(アニメーションで矩形を移動させるときに
	//この値を変更する)
	int [] LX = new int[5];
	int [] LY = new int[5];
	int [] RX = new int[5];
	int [] RY = new int[5];

	RectAnimationData L;
	RectAnimationData R;

	public SuperPosingCanvas(RectAnimationData L, RectAnimationData R) {
		for(int i = 0; i < L.input.length; ++i) {
			LX[i] = LY[i] = RX[i] = RY[i] = 0;
		}

		for(int i=0;i<L.input.length;i++){
			for(int j=0;j<L.input[i].length;j++){
				if(L.input[i][j]!=0){
					LX[j]=50;
					LY[i]=50;
				}
				if(R.input[i][j]!=0){
					RX[j]=50;
					RY[i]=50;
				}
			}
		}

		LeftX=10;
		LeftY=10;
		RightX=310;
		RightY=10;

		this.L = L.clone();
		this.R = R.clone();
	}

	public void run(){
		try{
			int trueCnt=0;
			//サイズの縮小(縮小するsubregionがないなら無視)
			for(int i=0;i<L.coreY.length;i++){
				if(L.coreY[i]==true || L.coreX[i]==true ||
						R.coreX[i]==true ||R.coreY[i]==true){
					trueCnt++;
				}
			}
			if(trueCnt!=0){
				for(int i=50;i>0;i--){

					for(int s=0;s<L.coreY.length;s++){
						if(L.coreY[s]==true){
								LY[s]--;
						}
						if(L.coreX[s]==true){
								LX[s]--;
						}
					}
					for(int s=0;s<R.coreY.length;s++){
						if(R.coreY[s]==true){
								RY[s]--;
						}
						if(R.coreX[s]==true){
								RX[s]--;
						}
					}
					//----------------------------
					repaint();
					Thread.sleep(20);
					//----------------------------
				}
			}
			//unit1の移動
			// todo : RectAnimationDataのmoveXやmoveYを使ってできないか
			for(int i=200;i>0;i--){
				LeftX++;
				LeftY++;
				repaint();
				Thread.sleep(10);
			}
			//unit2の移動
			if(R.moveX*50>300){
				int k=R.moveX*50-300;
				for(int i=k;i>0;i--){
					RightX++;
					repaint();
					Thread.sleep(5);
				}
			}
			else{
				int k=300-R.moveX*50;

				for(int i=k;i>0;i--){
					RightX--;
					repaint();
					Thread.sleep(5);
				}
			}
			if(R.moveY*50+10>10){
				int k=R.moveY*50;
				for(int i=k;i>0;i--){
					RightY++;
					repaint();
					Thread.sleep(5);
				}
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
	}

	public void update(Graphics g) {

		//消さずに更新

		paint(g);
	}

	public void paint(Graphics g){

		//unit1,2の描画
		paintUnit(g);
	}

	public void paintUnit(Graphics g){

		//アニメーションで矩形が動くときのちらつきを解消するために
		//ダブルバッファリングを用いています。


		//簡潔な書き方が思いつかなかったので、
		//かなり冗長なコードになってます。ごめんなさい。


		if(size == null || !size.equals(getSize())) {
			size = getSize();
			back = createImage(size.width, size.height);
		}
		buffer = back.getGraphics();

		buffer.clearRect(0, 0, size.width, size.height);

		//-------------左の矩形(black)---------------

		//animeinput[ ][ ]==1(白色)
		//animeinput[ ][ ]==2(黒色)
		buffer.setColor(Color.black);
		if(L.input[0][0] == 2) {
			buffer.fillRect(LeftX,LeftY,LX[0],LY[0]);
		}
		if(L.input[0][1] == 2) {
			buffer.fillRect(LeftX+LX[0],LeftY,LX[1],LY[0]);
		}
		if(L.input[0][2] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1],LeftY,LX[2],LY[0]);
		}
		if(L.input[0][3] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2],LeftY,LX[3],LY[0]);
		}
		if(L.input[0][4] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2]+LX[3],LeftY,LX[4],LY[0]);
		}

		if(L.input[1][0] == 2) {
			buffer.fillRect(LeftX,LeftY+LY[0],LX[0],LY[1]);
		}
		if(L.input[1][1] == 2) {
			buffer.fillRect(LeftX+LX[0],LeftY+LY[0],LX[1],LY[1]);
		}
		if(L.input[1][2] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1],LeftY+LY[0],LX[2],LY[1]);
		}
		if(L.input[1][3] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2],LeftY+LY[0],LX[3],LY[1]);
		}
		if(L.input[1][4] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2]+LX[3],LeftY+LY[0],LX[4],LY[1]);
		}


		if(L.input[2][0] == 2) {
			buffer.fillRect(LeftX,LeftY+LY[0]+LY[1],LX[0],LY[2]);
		}
		if(L.input[2][1] == 2) {
			buffer.fillRect(LeftX+LX[0],LeftY+LY[0]+LY[1],LX[1],LY[2]);
		}
		if(L.input[2][2] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1],LeftY+LY[0]+LY[1],LX[2],LY[2]);
		}
		if(L.input[2][3] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2],LeftY+LY[0]+LY[1],LX[3],LY[2]);
		}
		if(L.input[2][4] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2]+LX[3],LeftY+LY[0]+LY[1],LX[4],LY[2]);
		}


		if(L.input[3][0] == 2) {
			buffer.fillRect(LeftX,LeftY+LY[0]+LY[1]+LY[2],LX[0],LY[3]);
		}
		if(L.input[3][1] == 2) {
			buffer.fillRect(LeftX+LX[0],LeftY+LY[0]+LY[1]+LY[2],LX[1],LY[3]);
		}
		if(L.input[3][2] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1],LeftY+LY[0]+LY[1]+LY[2],LX[2],LY[3]);
		}
		if(L.input[3][3] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2],LeftY+LY[0]+LY[1]+LY[2],LX[3],LY[3]);
		}
		if(L.input[3][4] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2]+LX[3],LeftY+LY[0]+LY[1]+LY[2],LX[4],LY[3]);
		}

		if(L.input[4][0] == 2) {
			buffer.fillRect(LeftX,LeftY+LY[0]+LY[1]+LY[2]+LY[3],LX[0],LY[4]);
		}
		if(L.input[4][1] == 2) {
			buffer.fillRect(LeftX+LX[0],LeftY+LY[0]+LY[1]+LY[2]+LY[3],LX[1],LY[4]);
		}
		if(L.input[4][2] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1],LeftY+LY[0]+LY[1]+LY[2]+LY[3],LX[2],LY[4]);
		}
		if(L.input[4][3] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2],LeftY+LY[0]+LY[1]+LY[2]+LY[3],LX[3],LY[4]);
		}
		if(L.input[4][4] == 2) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2]+LX[3],LeftY+LY[0]+LY[1]+LY[2]+LY[3],LX[4],LY[4]);
		}

		//-------------左の矩形(white)---------------
		buffer.setColor(Color.white);
		if(L.input[0][0] == 1) {
			buffer.fillRect(LeftX,LeftY,LX[0],LY[0]);
		}
		if(L.input[0][1] == 1) {
			buffer.fillRect(LeftX+LX[0],LeftY,LX[1],LY[0]);
		}
		if(L.input[0][2] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1],LeftY,LX[2],LY[0]);
		}
		if(L.input[0][3] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2],LeftY,LX[3],LY[0]);
		}
		if(L.input[0][4] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2]+LX[3],LeftY,LX[4],LY[0]);
		}


		if(L.input[1][0] == 1) {
			buffer.fillRect(LeftX,LeftY+LY[0],LX[0],LY[1]);
		}
		if(L.input[1][1] == 1) {
			buffer.fillRect(LeftX+LX[0],LeftY+LY[0],LX[1],LY[1]);
		}
		if(L.input[1][2] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1],LeftY+LY[0],LX[2],LY[1]);
		}
		if(L.input[1][3] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2],LeftY+LY[0],LX[3],LY[1]);
		}
		if(L.input[1][4] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2]+LX[3],LeftY+LY[0],LX[4],LY[1]);
		}


		if(L.input[2][0] == 1) {
			buffer.fillRect(LeftX,LeftY+LY[0]+LY[1],LX[0],LY[2]);
		}
		if(L.input[2][1] == 1) {
			buffer.fillRect(LeftX+LX[0],LeftY+LY[0]+LY[1],LX[1],LY[2]);
		}
		if(L.input[2][2] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1],LeftY+LY[0]+LY[1],LX[2],LY[2]);
		}
		if(L.input[2][3] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2],LeftY+LY[0]+LY[1],LX[3],LY[2]);
		}
		if(L.input[2][4] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2]+LX[3],LeftY+LY[0]+LY[1],LX[4],LY[2]);
		}

		if(L.input[3][0] == 1) {
			buffer.fillRect(LeftX,LeftY+LY[0]+LY[1]+LY[2],LX[0],LY[3]);
		}
		if(L.input[3][1] == 1) {
			buffer.fillRect(LeftX+LX[0],LeftY+LY[0]+LY[1]+LY[2],LX[1],LY[3]);
		}
		if(L.input[3][2] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1],LeftY+LY[0]+LY[1]+LY[2],LX[2],LY[3]);
		}
		if(L.input[3][3] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2],LeftY+LY[0]+LY[1]+LY[2],LX[3],LY[3]);
		}
		if(L.input[3][4] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2]+LX[3],LeftY+LY[0]+LY[1]+LY[2],LX[4],LY[3]);
		}

		if(L.input[4][0] == 1) {
			buffer.fillRect(LeftX,LeftY+LY[0]+LY[1]+LY[2]+LY[3],LX[0],LY[4]);
		}
		if(L.input[4][1] == 1) {
			buffer.fillRect(LeftX+LX[0],LeftY+LY[0]+LY[1]+LY[2]+LY[3],LX[1],LY[4]);
		}
		if(L.input[4][2] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1],LeftY+LY[0]+LY[1]+LY[2]+LY[3],LX[2],LY[4]);
		}
		if(L.input[4][3] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2],LeftY+LY[0]+LY[1]+LY[2]+LY[3],LX[3],LY[4]);
		}
		if(L.input[4][4] == 1) {
			buffer.fillRect(LeftX+LX[0]+LX[1]+LX[2]+LX[3],LeftY+LY[0]+LY[1]+LY[2]+LY[3],LX[4],LY[4]);
		}

		//矩形を太い緑の線で囲う
		Graphics2D buffer5 = (Graphics2D)buffer;
		BasicStroke wideStroke3 = new BasicStroke(6.0f);
		buffer5.setStroke(wideStroke3);
		buffer5.setColor(Color.GREEN);
		buffer5.drawRect(LeftX, LeftY, LX[0]+LX[1]+LX[2]+LX[3]+LX[4], LY[0]+LY[1]+LY[2]+LY[3]+LY[4]);

		//-------------右の矩形(black)---------------

		//animeinput[ ][ ]==1(白色)
		//animeinput[ ][ ]==2(黒色)
		buffer.setColor(Color.black);
		if(R.input[0][0] == 2) {
			buffer.fillRect(RightX,RightY,RX[0],RY[0]);
		}
		if(R.input[0][1] == 2) {
			buffer.fillRect(RightX+RX[0],RightY,RX[1],RY[0]);
		}
		if(R.input[0][2] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1],RightY,RX[2],RY[0]);
		}
		if(R.input[0][3] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2],RightY,RX[3],RY[0]);
		}
		if(R.input[0][4] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2]+RX[3],RightY,RX[4],RY[0]);
		}


		if(R.input[1][0] == 2) {
			buffer.fillRect(RightX,RightY+RY[0],RX[0],RY[1]);
		}
		if(R.input[1][1] == 2) {
			buffer.fillRect(RightX+RX[0],RightY+RY[0],RX[1],RY[1]);
		}
		if(R.input[1][2] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1],RightY+RY[0],RX[2],RY[1]);
		}
		if(R.input[1][3] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2],RightY+RY[0],RX[3],RY[1]);
		}
		if(R.input[1][4] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2]+RX[3],RightY+RY[0],RX[4],RY[1]);
		}


		if(R.input[2][0] == 2) {
			buffer.fillRect(RightX,RightY+RY[0]+RY[1],RX[0],RY[2]);
		}
		if(R.input[2][1] == 2) {
			buffer.fillRect(RightX+RX[0],RightY+RY[0]+RY[1],RX[1],RY[2]);
		}
		if(R.input[2][2] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1],RightY+RY[0]+RY[1],RX[2],RY[2]);
		}
		if(R.input[2][3] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2],RightY+RY[0]+RY[1],RX[3],RY[2]);
		}
		if(R.input[2][4] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2]+RX[3],RightY+RY[0]+RY[1],RX[4],RY[2]);
		}

		if(R.input[3][0] == 2) {
			buffer.fillRect(RightX,RightY+RY[0]+RY[1]+RY[2],RX[0],RY[3]);
		}
		if(R.input[3][1] == 2) {
			buffer.fillRect(RightX+RX[0],RightY+RY[0]+RY[1]+RY[2],RX[1],RY[3]);
		}
		if(R.input[3][2] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1],RightY+RY[0]+RY[1]+RY[2],RX[2],RY[3]);
		}
		if(R.input[3][3] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2],RightY+RY[0]+RY[1]+RY[2],RX[3],RY[3]);
		}
		if(R.input[3][4] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2]+RX[3],RightY+RY[0]+RY[1]+RY[2],RX[4],RY[3]);
		}

		if(R.input[4][0] == 2) {
			buffer.fillRect(RightX,RightY+RY[0]+RY[1]+RY[2]+RY[3],RX[0],RY[4]);
		}
		if(R.input[4][1] == 2) {
			buffer.fillRect(RightX+RX[0],RightY+RY[0]+RY[1]+RY[2]+RY[3],RX[1],RY[4]);
		}
		if(R.input[4][2] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1],RightY+RY[0]+RY[1]+RY[2]+RY[3],RX[2],RY[4]);
		}
		if(R.input[4][3] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2],RightY+RY[0]+RY[1]+RY[2]+RY[3],RX[3],RY[4]);
		}
		if(R.input[4][4] == 2) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2]+RX[3],RightY+RY[0]+RY[1]+RY[2]+RY[3],RX[4],RY[4]);
		}


		//-------------右の矩形(white)---------------
		buffer.setColor(Color.white);
		if(R.input[0][0] == 1) {
			buffer.fillRect(RightX,RightY,RX[0],RY[0]);
		}
		if(R.input[0][1] == 1) {
			buffer.fillRect(RightX+RX[0],RightY,RX[1],RY[0]);
		}
		if(R.input[0][2] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1],RightY,RX[2],RY[0]);
		}
		if(R.input[0][3] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2],RightY,RX[3],RY[0]);
		}
		if(R.input[0][4] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2]+RX[3],RightY,RX[4],RY[0]);
		}


		if(R.input[1][0] == 1) {
			buffer.fillRect(RightX,RightY+RY[0],RX[0],RY[1]);
		}
		if(R.input[1][1] == 1) {
			buffer.fillRect(RightX+RX[0],RightY+RY[0],RX[1],RY[1]);
		}
		if(R.input[1][2] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1],RightY+RY[0],RX[2],RY[1]);
		}
		if(R.input[1][3] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2],RightY+RY[0],RX[3],RY[1]);
		}
		if(R.input[1][4] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2]+RX[3],RightY+RY[0],RX[4],RY[1]);
		}


		if(R.input[2][0] == 1) {
			buffer.fillRect(RightX,RightY+RY[0]+RY[1],RX[0],RY[2]);
		}
		if(R.input[2][1] == 1) {
			buffer.fillRect(RightX+RX[0],RightY+RY[0]+RY[1],RX[1],RY[2]);
		}
		if(R.input[2][2] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1],RightY+RY[0]+RY[1],RX[2],RY[2]);
		}
		if(R.input[2][3] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2],RightY+RY[0]+RY[1],RX[3],RY[2]);
		}
		if(R.input[2][4] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2]+RX[3],RightY+RY[0]+RY[1],RX[4],RY[2]);
		}


		if(R.input[3][0] == 1) {
			buffer.fillRect(RightX,RightY+RY[0]+RY[1]+RY[2],RX[0],RY[3]);
		}
		if(R.input[3][1] == 1) {
			buffer.fillRect(RightX+RX[0],RightY+RY[0]+RY[1]+RY[2],RX[1],RY[3]);
		}
		if(R.input[3][2] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1],RightY+RY[0]+RY[1]+RY[2],RX[2],RY[3]);
		}
		if(R.input[3][3] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2],RightY+RY[0]+RY[1]+RY[2],RX[3],RY[3]);
		}
		if(R.input[3][4] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2]+RX[3],RightY+RY[0]+RY[1]+RY[2],RX[4],RY[3]);
		}


		if(R.input[4][0] == 1) {
			buffer.fillRect(RightX,RightY+RY[0]+RY[1]+RY[2]+RY[3],RX[0],RY[4]);
		}
		if(R.input[4][1] == 1) {
			buffer.fillRect(RightX+RX[0],RightY+RY[0]+RY[1]+RY[2]+RY[3],RX[1],RY[4]);
		}
		if(R.input[4][2] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1],RightY+RY[0]+RY[1]+RY[2]+RY[3],RX[2],RY[4]);
		}
		if(R.input[4][3] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2],RightY+RY[0]+RY[1]+RY[2]+RY[3],RX[3],RY[4]);
		}
		if(R.input[4][4] == 1) {
			buffer.fillRect(RightX+RX[0]+RX[1]+RX[2]+RX[3],RightY+RY[0]+RY[1]+RY[2]+RY[3],RX[4],RY[4]);
		}

		//矩形を太い青の線で囲う
		buffer5.setColor(Color.blue);
		buffer5.drawRect(RightX,RightY,RX[0]+RX[1]+RX[2]+RX[3]+RX[4],RY[0]+RY[1]+RY[2]+RY[3]+RY[4]);

		//バッファ領域に書いていた情報を
		//フレームに描画する。
		g.drawImage(back,0,0,this);

	}

}
