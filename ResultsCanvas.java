import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;

class ResultsCanvas extends Canvas{

	//重ねた図形の描画領域
	int [][][]results = new int[10][14][14];

	RectAnimationData [] LAnimations = new RectAnimationData[10];
	RectAnimationData [] RAnimations = new RectAnimationData[10];

	// 結果、左側の動き方情報一覧、右側の動き方情報一覧
	public ResultsCanvas (int [][][] results, RectAnimationData [] LAnimations, RectAnimationData [] RAnimations) {
		for(int i = 0; i < results.length; ++i) {
			for(int j = 0; j < results[i].length; ++j) {
				for(int k = 0; k < results[i][j].length; ++k) {
					this.results[i][j][k] = results[i][j][k];
				}
			}
		}

		for(int i = 0; i < LAnimations.length; ++i) {
			this.LAnimations[i] = LAnimations[i].clone();
			this.RAnimations[i] = RAnimations[i].clone();
		}

		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent ev) {
				int mou_x2=ev.getX();//x座標の保存
				int mou_y2=ev.getY();//y座標の保存

				int resultNmber = pressedPart(mou_x2, mou_y2);

				if(resultNmber>= 0 && resultNmber < 10 && !allZero(results[resultNmber])){

					makeResultFrame(resultNmber);
				}
				else{
					msg("きゃっ、そんなとこ押さないで");
				}
			}
		});
	}
	public void makeResultFrame(int resultNmber){

		//アニメーションを表示するウィンドウ
		JFrame reframe = new JFrame();

		final Color backc = Color.GRAY;//一番奥の背景色
		final int Width = 600, Height = 600;//フレームの大

		reframe.setBounds(0,0,Width,Height);
		reframe.setBackground(Color.blue);
		// reframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//×ボタンでウィンドウを閉じる // 閉じるな

		//格子が書いてある描画領域
		SuperPosingCanvas c3 = new SuperPosingCanvas(LAnimations[resultNmber], RAnimations[resultNmber]);
		c3.setBounds(0,0,Width,Height);
		c3.setBackground(backc);
		reframe.add(c3);

		reframe.setVisible(true);

		Thread t1 = new Thread(c3);
		t1.start();

		c3.repaint(); // ??
	}

	private static boolean allZero(int[][]array){

		//arrayの中が全部0ならtrueを返す

		int roopCnt=0;
		int sameCnt=0;

		for(int i=0;i<array.length;i++){
			for(int j=0;j<array[0].length;j++){

				roopCnt++;

				if(array[i][j]==0){
					sameCnt++;
				}
			}
		}
		if(sameCnt==roopCnt){
			return true;
		}
		return false;
	}


	private static int pressedPart(int x, int y) {

		//マウスクリック時の処理

		int resultNmber = -1;
		//menuフレームに表示される左側の格子5個を押したときの処理
		if(x>=11&&x<=140+11){
			if(y>=7&&y<=7+140){
				resultNmber=0;
			}
			if(y>=7+150&&y<=7+290){
				resultNmber=2;
			}
			if(y>=7+300&&y<=7+440){
				resultNmber=4;
			}
			if(y>=7+450&&y<=7+590){
				resultNmber=6;
			}
			if(y>=7+600&&y<=7+740){
				resultNmber=8;
			}
		}
		////menuフレームに表示される右側の格子5個を押したときの処理
		if(x>=150+11&&x<=290+11){
			if(y>=7&&y<=7+140){
				resultNmber=1;
			}
			if(y>=7+150&&y<=7+290){
				resultNmber=3;
			}
			if(y>=7+300&&y<=7+440){
				resultNmber=5;
			}
			if(y>=7+450&&y<=7+590){
				resultNmber=7;
			}
			if(y>=7+600&&y<=7+740){
				resultNmber=9;
			}
		}
		return resultNmber;
	}




	@Override
	public void paint(Graphics g){

		//格子の描画
		paintRect(g);

		for(int i = 0; i < 10; ++i) {
			paintColor(g, results[i], i%2, i/2); // 2列5段
		}
	}

	private static void paintColor(Graphics g,int[][] array,int t,int s){

		//配列の中を見て、矩形を色塗り
		//1:白、2:黒

		int x=11;
		int y=7;
		int w=10;

		for(int i=0;i<14;++i) {
			for(int j=0;j<14;++j) {
				//白にぬる
				if(array[i][j] == 1) {
					g.setColor(Color.white);
					g.fillRect(x+w*j+(t*150),y+w*i+(s*150),w,w);
				}
				//黒にぬる
				if(array[i][j] == 2) {
					g.setColor(Color.black);
					g.fillRect(x+w*j+(t*150),y+w*i+(s*150),w,w);
				}
			}
		}
	}

	private static void paintRect(Graphics g){

		//menuフレームに書かれている10個の格子を書く

		int x=11;
		int y=7;
		int w=10;
		int x2=x;
		int y2=y;

		//格子を描画する
		for(int s=0;s<5;s++){
			for(int k=0;k<2;k++){
				for (int i=1; i<=14; i++) {
					for (int j=1; j<=14; j++) {
						g.setColor(Color.GRAY);
						g.fillRect(x+k*150,y+150*s,w,w);
						g.setColor(Color.black);
						g.drawRect(x+k*150,y+150*s,w,w);
						x=x+w;
					}
					x=x2;
					y=y+w;
				}y=y2;
			}
		}

	}

	private static void msg(String msg){

		//コメント書く用

		System.out.println(msg);
	}

}

